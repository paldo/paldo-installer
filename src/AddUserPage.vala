/* AddUserPage.vala
 *
 * Copyright (C) 2007  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

using GLib;
using Gtk;

public class Paldo.AddUserPage : VBox {
	public InstallerSettings settings { get; set; }

	private Entry username_entry;
	private SpinButton uid_spinbutton;
	private Entry fullname_entry;
	private Entry password_entry;
	private Entry confirm_password_entry;

	private Regex username_regex;

	private bool complete;

	private const int MIN_UID = 1000;
	private const int MAX_UID = 65533;

	public AddUserPage (InstallerSettings settings) {
		this.settings = settings;
		spacing = 6;
		border_width = 12;

		username_regex = new Regex ("^[a-z_][a-z0-9_-]*$");


		var size_group = new SizeGroup (SizeGroupMode.HORIZONTAL);
		Label label;

		var section_label = new Label (_("<b>Identification</b>"));
		section_label.use_markup = true;
		section_label.xalign = 0;
		pack_start (section_label, false, false, 0);

		var hbox = new HBox (false, 6);
		pack_start (hbox, false, false, 0);
		hbox.pack_start (new Label ("    "), false, false, 0);
		label = new Label (_("User name:"));
		hbox.pack_start (label, false, false, 0);
		label.xalign = 0;
		size_group.add_widget (label);
		username_entry = new Entry ();
		username_entry.changed.connect (entry_changed);
		label.mnemonic_widget = username_entry;
		hbox.pack_start (username_entry, true, true, 6);

		label = new Label (_("User ID:"));
		hbox.pack_start (label, false, false, 0);
		label.xalign = 0;
		uid_spinbutton = new SpinButton.with_range (MIN_UID, MAX_UID, 1);
		uid_spinbutton.changed.connect (entry_changed);
		label.mnemonic_widget = uid_spinbutton;
		hbox.pack_start (uid_spinbutton, true, true, 6);

		hbox = new HBox (false, 6);
		pack_start (hbox, false, false, 0);
		hbox.pack_start (new Label ("    "), false, false, 0);
		label = new Label (_("Full name:"));
		hbox.pack_start (label, false, false, 0);
		label.xalign = 0;
		size_group.add_widget (label);
		fullname_entry = new Entry ();
		fullname_entry.changed.connect (entry_changed);
		label.mnemonic_widget = fullname_entry;
		hbox.pack_start (fullname_entry, true, true, 6);

		section_label = new Label (_("<b>Password</b>"));
		section_label.use_markup = true;
		section_label.xalign = 0;
		pack_start (section_label, false, false, 0);

		hbox = new HBox (false, 6);
		pack_start (hbox, false, false, 0);
		hbox.pack_start (new Label ("    "), false, false, 0);
		label = new Label (_("Password:"));
		hbox.pack_start (label, false, false, 0);
		label.xalign = 0;
		size_group.add_widget (label);
		password_entry = new Entry ();
		password_entry.visibility = false;
		password_entry.changed.connect (entry_changed);
		label.mnemonic_widget = password_entry;
		hbox.pack_start (password_entry, true, true, 6);

		hbox = new HBox (false, 6);
		pack_start (hbox, false, false, 0);
		hbox.pack_start (new Label ("    "), false, false, 0);
		label = new Label (_("Confirm password:"));
		hbox.pack_start (label, false, false, 0);
		label.xalign = 0;
		size_group.add_widget (label);
		confirm_password_entry = new Entry ();
		confirm_password_entry.visibility = false;
		confirm_password_entry.changed.connect (entry_changed);
		label.mnemonic_widget = confirm_password_entry;
		hbox.pack_start (confirm_password_entry, true, true, 6);
	}

	private void entry_changed (Editable editable) {
		settings.username = username_entry.text;
		settings.uid = ((int) uid_spinbutton.value).clamp (MIN_UID, MAX_UID);
		settings.full_name = fullname_entry.text;
		settings.user_password = password_entry.text;

		settings.changed ();

		bool new_complete = username_regex.match (username_entry.text) &&
		                    password_entry.text.length > 0 && password_entry.text == confirm_password_entry.text;
		if (complete != new_complete) {
			complete = new_complete;
			complete_changed (complete);
		}
	}

	public signal void complete_changed (bool complete);
}

