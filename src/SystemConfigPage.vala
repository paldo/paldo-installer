/* SystemConfigPage.vala
 *
 * Copyright (C) 2007  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

using GLib;
using Gtk;

public class Paldo.SystemConfigPage : VBox {
	public InstallerSettings settings { get; set; }

	private Entry hostname_entry;
	private Entry domainname_entry;
	private Entry password_entry;
	private Entry confirm_password_entry;

	private bool complete;
	
	private Regex hostname_regex;
	private Regex domainname_regex;

	public SystemConfigPage (InstallerSettings settings) {
		this.settings = settings;
		spacing = 6;
		border_width = 12;

		hostname_regex = new Regex ("^[a-z0-9]([a-z0-9-]*[a-z0-9])?$");
		domainname_regex = new Regex ("^[a-z0-9]([a-z0-9-]*[a-z0-9])?(\\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$");


		var table = new Table (9, 4, false);
		pack_start (table, false, false, 0);
		table.set_col_spacings (6);
		table.set_row_spacings (6);

		Label label;

		var section_label = new Label (_("<b>Network</b>"));
		section_label.use_markup = true;
		section_label.xalign = 0;
		table.attach (section_label, 0, 4, 0, 1, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		table.attach (new Label ("    "), 0, 1, 1, 5, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		label = new Label (_("Please choose a host and domain name for your system."));
		table.attach (label, 1, 4, 1, 2, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		label.xalign = 0;

		label = new Label (_("Host name:"));
		table.attach (label, 1, 2, 2, 3, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		label.xalign = 0;
		hostname_entry = new Entry ();
		hostname_entry.changed.connect (entry_changed);
		label.mnemonic_widget = hostname_entry;
		table.attach (hostname_entry, 2, 4, 2, 3, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		table.attach (new Image.from_stock (Gtk.Stock.DIALOG_INFO, IconSize.MENU), 2, 3, 3, 4, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		label = new Label (_("The host name may only contain lowercase letters, numbers, and dashes."));
		label.xalign = 0;
		table.attach (label, 3, 4, 3, 4, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		label = new Label (_("Domain name:"));
		table.attach (label, 1, 2, 4, 5, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		label.xalign = 0;
		domainname_entry = new Entry ();
		domainname_entry.text = "local";
		domainname_entry.changed.connect (entry_changed);
		label.mnemonic_widget = domainname_entry;
		table.attach (domainname_entry, 2, 4, 4, 5, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		table.attach (new Image.from_stock (Gtk.Stock.DIALOG_INFO, IconSize.MENU), 2, 3, 5, 6, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		label = new Label (_("The domain name may only contain lowercase letters, numbers, dashes, and dots."));
		label.xalign = 0;
		table.attach (label, 3, 4, 5, 6, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		section_label = new Label (_("<b>Root Password</b>"));
		section_label.use_markup = true;
		section_label.xalign = 0;
		table.attach (section_label, 0, 4, 6, 7, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		
		label = new Label (_("Password:"));
		table.attach (label, 1, 2, 7, 8, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		label.xalign = 0;
		password_entry = new Entry ();
		password_entry.visibility = false;
		password_entry.changed.connect (entry_changed);
		section_label.mnemonic_widget = password_entry;
		table.attach (password_entry, 2, 4, 7, 8, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		
		label = new Label (_("Confirm password:"));
		table.attach (label, 1, 2, 8, 9, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		label.xalign = 0;
		confirm_password_entry = new Entry ();
		confirm_password_entry.visibility = false;
		confirm_password_entry.changed.connect (entry_changed);
		label.mnemonic_widget = confirm_password_entry;
		table.attach (confirm_password_entry, 2, 4, 8, 9, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);
	}
	
	private void entry_changed (Editable editable) {
		settings.hostname = hostname_entry.text;
		settings.domainname = domainname_entry.text;
		settings.root_password = password_entry.text;

		settings.changed ();

		bool new_complete = hostname_regex.match (hostname_entry.text) && domainname_regex.match (domainname_entry.text) &&
		                    password_entry.text.length > 0 && password_entry.text == confirm_password_entry.text;
		if (complete != new_complete) {
			complete = new_complete;
			complete_changed (complete);
		}
	}

	public signal void complete_changed (bool complete);
}

