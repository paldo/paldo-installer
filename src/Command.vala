/* Command.vala
 *
 * Copyright (C) 2007-2008  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

using GLib;

public class Paldo.Command : Object {
	public string command_line { get; set; }
	public string? working_directory { get; set; }

	public Command (string command_line, string? working_directory = null) {
		this.command_line = command_line;
		this.working_directory = working_directory;
	}

	public bool run () {
		try {
			string[] argv = new string[4] { "sh", "-c", command_line, null };
			Pid pid;
			Process.spawn_async (working_directory, argv, null, SpawnFlags.DO_NOT_REAP_CHILD | SpawnFlags.SEARCH_PATH, null, out pid);
			ChildWatch.add_full (Priority.DEFAULT, pid, child_exit);
		} catch (SpawnError e) {
			critical ("command");
			return false;
		}
		return true;
	}

	private void child_exit (Pid pid, int status) {
		exit (status);
	}

	public signal void exit (int status);
}

