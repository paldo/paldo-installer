/* InstallPage.vala
 *
 * Copyright (C) 2007-2009  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

using GLib;
using Gtk;

public class Paldo.InstallPage : VBox {
	public InstallerSettings settings { get; set; }

	private ProgressBar progress_bar;
	private Label operation_label;

	// in KB
	private int free_space_at_start;

	private TimeVal time_at_start;

	private bool copy_running;

	private const int MISC_TICKS = 10;
	private const int FORMAT_TICKS = 1000;
	private const int COPY_TICKS = 10000;

	// ticks preceding the specified operation
	private int root_format_ticks;
	private int root_mount_ticks;
	private int boot_format_ticks;
	private int boot_mount_ticks;
	private int swap_format_ticks;
	private int copy_ticks;
	private int adjust_ticks;
	private int home_format_ticks;
	private int home_mount_ticks;
	private int system_config_ticks;
	private int add_user_ticks;
	private int unmount_ticks;
	private int total_ticks;

	public InstallPage (InstallerSettings settings) {
		this.settings = settings;
		spacing = 6;
		border_width = 12;

		var label = new Label (_("Installing paldo GNU/Linux"));
		label.use_markup = true;
		label.xalign = 0;
		pack_start (label, false, false, 0);

		progress_bar = new ProgressBar ();
		pack_start (progress_bar, false, false, 0);

		operation_label = new Label ("");
		operation_label.use_markup = true;
		operation_label.xalign = 0;
		pack_start (operation_label, false, false, 0);
	}

	/**
	 * Starts the installation.
	 */
	public void run () {
		this.settings.frozen = true;

		total_ticks = 0;

		root_format_ticks = total_ticks;
		total_ticks += FORMAT_TICKS;

		root_mount_ticks = total_ticks;
		total_ticks += MISC_TICKS;

		boot_format_ticks = total_ticks;
		total_ticks += FORMAT_TICKS;

		boot_mount_ticks = total_ticks;
		total_ticks += MISC_TICKS;

		swap_format_ticks = total_ticks;
		if (settings.swap_dev != null) {
			total_ticks += FORMAT_TICKS;
		}

		copy_ticks = total_ticks;
		total_ticks += COPY_TICKS;

		adjust_ticks = total_ticks;
		total_ticks += MISC_TICKS;

		home_format_ticks = total_ticks;
		if (settings.home_dev != null && settings.home_format) {
			total_ticks += FORMAT_TICKS;
		}

		home_mount_ticks = total_ticks;
		if (settings.home_dev != null) {
			total_ticks += MISC_TICKS;
		}

		system_config_ticks = total_ticks;
		total_ticks += MISC_TICKS;

		add_user_ticks = total_ticks;
		total_ticks += MISC_TICKS;

		unmount_ticks = total_ticks;
		total_ticks += MISC_TICKS;

		time_at_start.get_current_time ();

		update_progress (root_format_ticks, _("Formatting root partition"));

		var root_format_cmd = new Command ("mkfs.%s -q %s".printf (settings.root_fstype, Shell.quote (settings.root_dev)));
		root_format_cmd.exit.connect (root_format_exit);
		if (!root_format_cmd.run ()) {
			display_error (_("Unable to run command `%s`").printf (root_format_cmd.command_line));
		}
	}

	private void display_error (string message) {
		operation_label.label = _("Installation failed: %s").printf (message);
	}

	private void update_progress (int ticks, string operation) {
		progress_bar.fraction = (double) ticks / total_ticks;

		if (ticks == total_ticks) {
			progress_bar.text = "";
		} else if (ticks > 0) {
			var tv = TimeVal ();
			long remaining = (tv.tv_sec - time_at_start.tv_sec) * (total_ticks - ticks) / ticks;
			long remaining_min = (remaining + 30) / 60;
			if (remaining_min < 1) {
				progress_bar.text = _("Less than 1 minute left");
			} else if (remaining_min == 1) {
				progress_bar.text = _("About 1 minute left");
			} else if (remaining_min >= 60) {
				long remaining_h = (remaining + 1800) / 3600;
				if (remaining_h <= 1) {
					progress_bar.text = _("About 1 hour left");
				} else {
					progress_bar.text = _("About %ld hours left").printf (remaining_h);
				}
			} else {
				progress_bar.text = _("About %ld minutes left").printf (remaining_min);
			}
		}

		operation_label.label = "<i>%s</i>".printf (Markup.escape_text (operation));
	}

	private void root_format_exit (Command? cmd, int status) {
		if (status != 0) {
			display_error (_("Unable to format root partition."));
			return;
		}

		update_progress (root_mount_ticks, _("Mounting root partition"));

		var root_mount_cmd = new Command ("mount %s /upkg".printf (Shell.quote (settings.root_dev)));
		root_mount_cmd.exit.connect (root_mount_exit);
		if (!root_mount_cmd.run ()) {
			display_error (_("Unable to run command `%s`").printf (root_mount_cmd.command_line));
		}
	}

	private void root_mount_exit (Command? cmd, int status) {
		if (status != 0) {
			display_error (_("Unable to mount root partition."));
			return;
		}

		File f = File.new_for_path ("/upkg/");
		FileInfo fsinfo = f.query_filesystem_info (FileAttribute.FILESYSTEM_FREE, null);
		free_space_at_start = (int) (fsinfo.get_attribute_uint64 (FileAttribute.FILESYSTEM_FREE) / 1024);

		File boot_dir = File.new_for_path ("/upkg/boot");
		boot_dir.make_directory (null);
		
		update_progress (boot_format_ticks, _("Formatting boot partition"));

		var boot_format_cmd = new Command ("mkfs.vfat -F 32 %s".printf (Shell.quote (settings.boot_dev)));
		boot_format_cmd.exit.connect (boot_format_exit);
		if (!boot_format_cmd.run ()) {
			display_error (_("Unable to run command `%s`").printf (boot_format_cmd.command_line));
		}
	}

	private void boot_format_exit (Command? cmd, int status) {
		if (status != 0) {
			Process.spawn_command_line_sync ("umount -l /upkg");
			display_error (_("Unable to format boot partition."));
			return;
		}

		update_progress (boot_mount_ticks, _("Mounting boot partition"));

		var boot_mount_cmd = new Command ("mount %s /upkg/boot".printf (Shell.quote (settings.boot_dev)));
		boot_mount_cmd.exit.connect (boot_mount_exit);
		if (!boot_mount_cmd.run ()) {
			display_error (_("Unable to run command `%s`").printf (boot_mount_cmd.command_line));
		}
	}

	private void boot_mount_exit (Command? cmd, int status) {
		if (status != 0) {
			Process.spawn_command_line_sync ("umount -l /upkg");
			display_error (_("Unable to mount boot partition."));
			return;
		}

		if (settings.swap_dev == null) {
			swap_format_exit (null, 0);
		} else {
			update_progress (swap_format_ticks, _("Formatting swap partition"));

			var swap_format_cmd = new Command ("mkswap %s".printf (Shell.quote (settings.swap_dev)));
			swap_format_cmd.exit.connect (swap_format_exit);
			if (!swap_format_cmd.run ()) {
				display_error (_("Unable to run command `%s`").printf (swap_format_cmd.command_line));
			}
		}
	}

	private void swap_format_exit (Command? cmd, int status) {
		if (status != 0) {
			Process.spawn_command_line_sync ("umount -l /upkg/boot");
			Process.spawn_command_line_sync ("umount -l /upkg");
			display_error (_("Unable to format swap partition."));
			return;
		}

		update_progress (copy_ticks, _("Copying files, %.1f MB of %.1f MB copied").printf (0.0, (double) settings.installation_size / 1024));

		var unsquashfs_cmd = new Command ("unsquashfs -f -d /upkg /run/live/rootfs.img");
		unsquashfs_cmd.exit.connect (unsquashfs_exit);
		if (!unsquashfs_cmd.run ()) {
			display_error (_("Unable to run command `%s`").printf (unsquashfs_cmd.command_line));
			return;
		}
		copy_running = true;

		GLib.Timeout.add (1000, update_copy_progress);
	}

	[InstanceLast]
	private bool update_copy_progress () {
		if (!copy_running) {
			return false;
		}

		File f = File.new_for_path ("/upkg/");
		FileInfo fsinfo = f.query_filesystem_info (FileAttribute.FILESYSTEM_FREE, null);
		int free_space = (int) (fsinfo.get_attribute_uint64 (FileAttribute.FILESYSTEM_FREE) / 1024);

		int used_space = (free_space_at_start - free_space).clamp (0, settings.installation_size);
		// use double to avoid integer overflow
		int current_copy_ticks = ((int) ((double) COPY_TICKS * used_space / settings.installation_size)).clamp (0, COPY_TICKS);
		update_progress (copy_ticks + current_copy_ticks, _("Copying files, %.1f MB of %.1f MB copied").printf ((double) used_space / 1024, (double) settings.installation_size / 1024));

		return true;
	}

	private void unsquashfs_exit (Command? cmd, int status) {
		if (status != 0) {
			copy_running = false;
			Process.spawn_command_line_sync ("umount -l /upkg/boot");
			Process.spawn_command_line_sync ("umount -l /upkg");
			display_error (_("Error while copying files."));
			return;
		}

		// copy kernel and boot loader
		var boot_copy_cmd = new Command ("cp -a /run/live/{config-*,initramfs-*,linux-*,System.map-*,EFI,loader} /upkg/boot/");
		boot_copy_cmd.exit.connect (boot_copy_exit);
		if (!boot_copy_cmd.run ()) {
			display_error (_("Unable to run command `%s`").printf (boot_copy_cmd.command_line));
			return;
		}
	}

	private void boot_copy_exit (Command? cmd, int status) {
		copy_running = false;
		if (status != 0) {
			Process.spawn_command_line_sync ("umount -l /upkg/boot");
			Process.spawn_command_line_sync ("umount -l /upkg");
			display_error (_("Error while copying files."));
			return;
		}

		// revert live CD specific changes to the system configuration
		update_progress (adjust_ticks, _("Adjusting installation"));

		string resume_opt = "";
		if (settings.swap_dev != null) {
			resume_opt = " resume=@SWAP@";
		}

		var adjust_cmd = new Command ("rm -rf /upkg/{.readonly,home/paldo}"
			+ " && sed -i -e '/^paldo:/d' -e 's/:paldo$/:/' /upkg/etc/{passwd,group}"
			+ " && cp /upkg/usr/share/doc/gdm/custom.conf.paldo /upkg/etc/gdm/custom.conf"
			+ " && cp /upkg/usr/share/doc/util-linux/default-fstab.paldo /upkg/etc/fstab"
			+ " && sed -i -e 's/root=[^\"]*/root=@ROOTDEV@" + resume_opt + "/' /upkg/boot/loader/entries/*.conf /upkg/etc/kernel/cmdline"
			+ " && sed -i -e 's/cachedir chroot=\"yes\"/cachedir/' /upkg/etc/upkg.conf"
			+ " && chroot /upkg upkg-remove --force paldo-installer");
		adjust_cmd.exit.connect (adjust_exit);
		if (!adjust_cmd.run ()) {
			display_error (_("Unable to run command `%s`").printf (adjust_cmd.command_line));
			return;
		}
	}

	private void adjust_exit (Command? cmd, int status) {
		if (status != 0) {
			Process.spawn_command_line_sync ("umount -l /upkg/boot");
			Process.spawn_command_line_sync ("umount -l /upkg");
			display_error (_("Error while adjusting installation."));
			return;
		}

		if (settings.home_dev != null && settings.home_format) {
			update_progress (home_format_ticks, _("Formatting home partition"));

			var home_format_cmd = new Command ("mkfs.%s -q %s".printf (settings.home_fstype, Shell.quote (settings.home_dev)));
			home_format_cmd.exit.connect (home_format_exit);
			if (!home_format_cmd.run ()) {
				display_error (_("Unable to run command `%s`").printf (home_format_cmd.command_line));
			}
		} else {
			home_format_exit (null, 0);
		}
	}

	private void home_format_exit (Command? cmd, int status) {
		if (status != 0) {
			Process.spawn_command_line_sync ("umount -l /upkg/boot");
			Process.spawn_command_line_sync ("umount -l /upkg");
			display_error ("Error while mounting home partition.");
			return;
		}

		if (settings.home_dev != null) {
			update_progress (home_mount_ticks, _("Mounting home partition"));

			var home_mount_cmd = new Command ("mount %s /upkg/home".printf (Shell.quote (settings.home_dev)));
			home_mount_cmd.exit.connect (home_mount_exit);
			if (!home_mount_cmd.run ()) {
				display_error (_("Unable to run command `%s`").printf (home_mount_cmd.command_line));
			}
		} else {
			home_mount_exit (null, 0);
		}
	}

	private void home_mount_exit (Command? cmd, int status) {
		if (status != 0) {
			Process.spawn_command_line_sync ("umount -l /upkg/boot");
			Process.spawn_command_line_sync ("umount -l /upkg");
			display_error (_("Error while mounting home partition."));
			return;
		}

		update_progress (system_config_ticks, _("Writing system configuration"));

		// configure fstab and boot loader
		string fstab_sed = "sed -i -e \"s|@ROOTDEV@|/dev/disk/by-uuid/$(/sbin/blkid -o value -s UUID %s)|\" -e 's|@ROOTFSTYPE@|%s|'".printf (settings.root_dev, settings.root_fstype);
		fstab_sed += " -e \"s|^.*@BOOTDEV@|/dev/disk/by-uuid/$(/sbin/blkid -o value -s UUID %s)|\" -e 's|@BOOTFSTYPE@|%s|'".printf (settings.boot_dev, settings.boot_fstype);
		if (settings.home_dev != null) {
			fstab_sed += " -e \"s|# @HOMEDEV@|/dev/disk/by-uuid/$(/sbin/blkid -o value -s UUID %s)|\" -e 's|@HOMEFSTYPE@|%s|'".printf (settings.home_dev, settings.home_fstype);
		}
		if (settings.swap_dev != null) {
			fstab_sed += " -e \"s|# @SWAP@|/dev/disk/by-uuid/$(/sbin/blkid -o value -s UUID %s)|\" -e \"s|@SWAP@|/dev/disk/by-uuid/$(/sbin/blkid -o value -s UUID %s)|\"".printf (settings.swap_dev, settings.swap_dev);
		}
		fstab_sed += " /upkg/etc/fstab /upkg/boot/loader/entries/*.conf /upkg/etc/kernel/cmdline";

		// set hostname, root password, and locale settings
		var system_config_cmd = new Command (fstab_sed
			+ " && echo %s > /upkg/etc/hostname".printf (settings.hostname)
			+ " && echo %s > /upkg/etc/domainname".printf (settings.domainname)
			+ " && echo 127.0.0.1 %s.%s %s localhost > /upkg/etc/hosts".printf (settings.hostname, settings.domainname, settings.hostname)
			+ " && ( echo %s | chroot /upkg chpasswd )".printf (Shell.quote ("root:%s".printf (settings.root_password)))
			+ " && sed -i -e 's/LANG=.*/LANG=%s_%s.UTF-8/' /upkg/etc/profile.d/locale.sh".printf (settings.language, settings.country)
			+ " && echo LANG=%s_%s.UTF-8 > /upkg/etc/locale.conf".printf (settings.language, settings.country)
			+ " && echo KEYMAP=%s > /upkg/etc/vconsole.conf".printf (settings.kbd)
			+ " && localectl set-x11-keymap %s %s".printf (settings.xkblayout, settings.xkbvariant)
			+ " && install -D -m 0644 /etc/X11/xorg.conf.d/00-keyboard.conf /upkg/etc/X11/xorg.conf.d/00-keyboard.conf"
			+ " && cp /upkg/usr/share/zoneinfo/%s /upkg/etc/localtime".printf (settings.timezone));
		system_config_cmd.exit.connect (system_config_exit);
		if (!system_config_cmd.run ()) {
			display_error (_("Unable to run command `%s`").printf (system_config_cmd.command_line));
			return;
		}
	}

	private void system_config_exit (Command? cmd, int status) {
		if (status != 0) {
			if (settings.home_dev != null) {
				Process.spawn_command_line_sync ("umount -l /upkg/home");
			}
			Process.spawn_command_line_sync ("umount -l /upkg/boot");
			Process.spawn_command_line_sync ("umount -l /upkg");
			display_error (_("Error while writing system configuration."));
			return;
		}

		update_progress (add_user_ticks, _("Adding user"));

		var add_user_cmd = new Command ("chroot /upkg groupadd -g %d %s".printf (settings.uid, settings.username)
			+ " && chroot /upkg useradd -m -u %d -g %s -G audio,cdrom,network,video,wheel %s".printf (settings.uid, settings.username, settings.username)
			+ " && ( echo %s | chroot /upkg chpasswd )".printf (Shell.quote ("%s:%s".printf (settings.username, settings.user_password))));
		add_user_cmd.exit.connect (add_user_exit);
		if (!add_user_cmd.run ()) {
			display_error (_("Unable to run command `%s`").printf (add_user_cmd.command_line));
			return;
		}
	}

	private void add_user_exit (Command? cmd, int status) {
		update_progress (unmount_ticks, _("Unmounting partitions"));

		if (settings.home_dev != null) {
			Process.spawn_command_line_sync ("umount -l /upkg/home");
		}
		Process.spawn_command_line_sync ("umount -l /upkg/boot");
		Process.spawn_command_line_sync ("umount -l /upkg");

		if (status != 0) {
			display_error (_("Error while adding user."));
			return;
		}

		update_progress (total_ticks, _("Installation finished."));

		complete_changed (true);
	}

	public signal void complete_changed (bool complete);
}

