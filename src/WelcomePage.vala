/* WelcomePage.vala
 *
 * Copyright (C) 2007-2008  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

using GLib;
using Gtk;

public class Paldo.WelcomePage : VBox {
	public InstallerSettings settings { get; set; }

	private Gtk.ListStore language_model;
	private ComboBox language_combobox;
	private Gtk.ListStore country_model;
	private ComboBox country_combobox;
	private Gtk.ListStore kbd_model;
	private ComboBox kbd_combobox;
	private Gtk.ListStore tz_model;
	private ComboBox tz_combobox;
	private TreeModelFilter country_filter;

	private int n_languages;
	private int n_countries;
	private HashTable<string,int> language_code_table = new HashTable<string,int>.full (str_hash, str_equal, g_free, null);
	private HashTable<string,int> country_code_table = new HashTable<string,int>.full (str_hash, str_equal, g_free, null);
	private bool[] language_countries;
	private int[] language_default_country;
	private int n_keyboard_layouts;
	private int n_timezones;

	public WelcomePage (InstallerSettings settings) {
		this.settings = settings;
		spacing = 6;
		border_width = 12;

		var label = new Label (_("Welcome to the installation of paldo GNU/Linux."));
		label.xalign = 0;
		pack_start (label, false, false, 0);

		var section_label = new Label (_("<b>Language</b>"));
		section_label.use_markup = true;
		section_label.xalign = 0;
		pack_start (section_label, false, false, 0);

		var hbox = new HBox (false, 6);
		pack_start (hbox, false, false, 0);
		hbox.pack_start (new Label ("    "), false, false, 0);
		
		string current_locale = Environment.get_variable ("LANG");

		language_model = new Gtk.ListStore (2, typeof (string), typeof (string));
		add_language ("en", _("English"));
		add_language ("fr", _("French"));
		add_language ("de", _("German"));
		add_language ("hu", _("Hungarian"));
		add_language ("it", _("Italian"));
		add_language ("pl", _("Polish"));
		add_language ("pt", _("Portuguese"));
		add_language ("es", _("Spanish"));
		add_language ("tr", _("Turkish"));

		language_combobox = new ComboBox.with_model (language_model);
		hbox.pack_start (language_combobox, false, false, 6);
		var renderer = new CellRendererText ();
		language_combobox.pack_start (renderer, true);
		language_combobox.add_attribute (renderer, "text", 1);
		language_combobox.changed.connect (language_changed);
		section_label.mnemonic_widget = language_combobox;

		country_model = new Gtk.ListStore (4, typeof (string), typeof (string), typeof (Gdk.Pixbuf), typeof (bool));
		add_country ("AR", _("Argentinia"));
		add_country ("AU", _("Australia"));
		add_country ("AT", _("Austria"));
		add_country ("BE", _("Belgium"));
		add_country ("BO", _("Bolivia"));
		add_country ("BR", _("Brazil"));
		add_country ("CA", _("Canada"));
		add_country ("CL", _("Chile"));
		add_country ("CO", _("Colombia"));
		add_country ("CR", _("Costa Rica"));
		add_country ("DO", _("Dominican Republic"));
		add_country ("EC", _("Ecuador"));
		add_country ("SV", _("El Salvador"));
		add_country ("FR", _("France"));
		add_country ("DE", _("Germany"));
		add_country ("GT", _("Guatemala"));
		add_country ("HN", _("Honduras"));
		add_country ("HU", _("Hungary"));
		add_country ("IE", _("Ireland"));
		add_country ("IT", _("Italy"));
		add_country ("LU", _("Luxembourg"));
		add_country ("MX", _("Mexico"));
		add_country ("NI", _("Nicaragua"));
		add_country ("NZ", _("New Zealand"));
		add_country ("PA", _("Panama"));
		add_country ("PY", _("Paraguay"));
		add_country ("PE", _("Peru"));
		add_country ("PL", _("Poland"));
		add_country ("PT", _("Portugal"));
		add_country ("PR", _("Puerto Rico"));
		add_country ("ES", _("Spain"));
		add_country ("CH", _("Switzerland"));
		add_country ("TR", _("Turkey"));
		add_country ("GB", _("United Kingdom"));
		add_country ("US", _("United States"));
		add_country ("UY", _("Uruguay"));
		add_country ("VE", _("Venezuela"));
		
		country_filter = (TreeModelFilter) new TreeModelFilter (country_model, null);
		country_filter.set_visible_column (3);

		country_combobox = new ComboBox.with_model (country_filter);
		hbox.pack_start (country_combobox, true, true, 6);
		var renderer_pixbuf = new CellRendererPixbuf ();
		country_combobox.pack_start (renderer_pixbuf, false);
		country_combobox.add_attribute (renderer_pixbuf, "pixbuf", 2);
		renderer = new CellRendererText ();
		country_combobox.pack_start (renderer, true);
		country_combobox.add_attribute (renderer, "text", 1);
		country_combobox.changed.connect (country_changed);

		language_countries = new bool[n_languages * n_countries];
		language_default_country = new int[n_languages];
		
		add_locale ("de", "AT");
		add_locale ("de", "BE");
		add_locale ("de", "CH");
		add_locale ("de", "DE", true);
		add_locale ("de", "LU");
		add_locale ("en", "AU");
		add_locale ("en", "CA");
		add_locale ("en", "CH");
		add_locale ("en", "GB");
		add_locale ("en", "IE");
		add_locale ("en", "NZ");
		add_locale ("en", "US", true);
		// add_locale ("es", "AR");
		// add_locale ("es", "BO");
		// add_locale ("es", "CL");
		// add_locale ("es", "CO");
		// add_locale ("es", "CR");
		// add_locale ("es", "DO");
		// add_locale ("es", "EC");
		add_locale ("es", "ES", true);
		// add_locale ("es", "GT");
		// add_locale ("es", "HN");
		add_locale ("es", "MX");
		// add_locale ("es", "NI");
		// add_locale ("es", "PA");
		// add_locale ("es", "PE");
		// add_locale ("es", "PR");
		// add_locale ("es", "PY");
		// add_locale ("es", "SV");
		add_locale ("es", "US");
		// add_locale ("es", "UY");
		// add_locale ("es", "VE");
		add_locale ("fr", "BE");
		add_locale ("fr", "CA");
		add_locale ("fr", "CH");
		add_locale ("fr", "FR", true);
		add_locale ("fr", "LU");
		add_locale ("hu", "HU", true);
		add_locale ("it", "CH");
		add_locale ("it", "IT", true);
		add_locale ("pl", "PL", true);
		add_locale ("pt", "BR");
		add_locale ("pt", "PT", true);
		add_locale ("tr", "TR", true);

		section_label = new Label (_("<b>Keyboard Layout</b>"));
		section_label.use_markup = true;
		section_label.xalign = 0;
		pack_start (section_label, false, false, 0);

		hbox = new HBox (false, 6);
		pack_start (hbox, false, false, 0);
		hbox.pack_start (new Label ("    "), false, false, 0);

		kbd_model = new Gtk.ListStore (4, typeof (string), typeof (string), typeof (string), typeof (string));
		int kbd_br = add_keyboard_layout ("br", "", "br-abnt2", _("Brazilian"));
		int kbd_gb = add_keyboard_layout ("gb", "", "uk", _("British"));
		int kbd_ca = add_keyboard_layout ("ca", "", "cf", _("Canadian"));
		int kbd_fr = add_keyboard_layout ("fr", "", "fr-latin9", _("French"));
		int kbd_de = add_keyboard_layout ("de", "", "de-latin1", _("German"));
		int kbd_hu = add_keyboard_layout ("hu", "", "hu", _("Hungarian"));
		int kbd_it = add_keyboard_layout ("it", "", "it", _("Italian"));
		int kbd_pl = add_keyboard_layout ("pl", "", "pl", _("Polish"));
		int kbd_pt = add_keyboard_layout ("pt", "", "pt-latin9", _("Portuguese"));
		int kbd_es = add_keyboard_layout ("es", "", "es", _("Spanish"));
		int kbd_ch_fr = add_keyboard_layout ("ch", "fr", "fr_CH-latin1", _("Swiss French"));
		int kbd_ch = add_keyboard_layout ("ch", "de_nodeadkeys", "sg-latin1", _("Swiss German"));
		int kbd_tr = add_keyboard_layout ("tr", "", "tr", _("Turkish"));
		int kbd_us = add_keyboard_layout ("us", "", "us", _("United States"));

		kbd_combobox = new ComboBox.with_model (kbd_model);
		hbox.pack_start (kbd_combobox, true, true, 6);
		renderer = new CellRendererText ();
		kbd_combobox.pack_start (renderer, true);
		kbd_combobox.add_attribute (renderer, "text", 3);
		kbd_combobox.changed.connect (kbd_changed);
		section_label.mnemonic_widget = kbd_combobox;

		section_label = new Label (_("<b>Time Zone</b>"));
		section_label.use_markup = true;
		section_label.xalign = 0;
		pack_start (section_label, false, false, 0);

		hbox = new HBox (false, 6);
		pack_start (hbox, false, false, 0);
		hbox.pack_start (new Label ("    "), false, false, 0);

		tz_model = new Gtk.ListStore (2, typeof (string), typeof (string));
		add_time_zone ("America/Los_Angeles", _("UTC-08:00: North American Pacific Time: Los Angeles"));
		add_time_zone ("America/Denver", _("UTC-07:00: North American Mountain Time: Denver"));
		add_time_zone ("America/Chicago", _("UTC-06:00: North American Central Time: Chicago"));
		add_time_zone ("America/New_York", _("UTC-05:00: North American Eastern Time: New York"));
		add_time_zone ("Europe/London", _("UTC+00:00: Western European Time: Dublin, Edinburgh, Lisbon, London"));
		int default_timezone = add_time_zone ("Europe/Zurich", _("UTC+01:00: Central European Time: Amsterdam, Berlin, Bern, Budapest, Rome, Stockholm, Vienna"));
		add_time_zone ("EET", _("UTC+02:00: Eastern European Time: Athens, Bucharest, Helsinki, Sofia"));
		add_time_zone ("Europe/Istanbul", _("UTC+03:00: Turkey Time: Istanbul"));
		add_time_zone ("Australia/Perth", _("UTC+08:00: Australian Western Time: Perth"));
		add_time_zone ("Australia/Adelaide", _("UTC+09:30: Australian Central Time: Adelaide"));
		add_time_zone ("Australia/Melbourne", _("UTC+10:00: Australian Eastern Time: Melbourne"));

		tz_combobox = new ComboBox.with_model (tz_model);
		hbox.pack_start (tz_combobox, true, true, 6);
		renderer = new CellRendererText ();
		tz_combobox.pack_start (renderer, true);
		tz_combobox.add_attribute (renderer, "text", 1);
		tz_combobox.active = default_timezone;
		tz_combobox.changed.connect (tz_changed);
		section_label.mnemonic_widget = tz_combobox;

		int current_lang;
		string current_lang_str = "en";
		if (current_locale != null && current_locale.length >= 2) {
			current_lang_str = current_locale.substring (0, 2);
		}
		current_lang = language_code_table.lookup (current_lang_str);
		language_combobox.active = current_lang;


		string current_country_str = "CH";
		if (current_locale != null && current_locale.length >= 5) {
			current_country_str = current_locale.substring (3, 2);
			int current_country = country_code_table.lookup (current_country_str);
			language_combobox.active = current_lang;

			TreeIter iter, filter_iter;
			country_model.iter_nth_child (out iter, null, current_country);
			country_filter.convert_child_iter_to_iter (out filter_iter, iter);
			country_combobox.set_active_iter (filter_iter);
		}

		kbd_combobox.active = kbd_us;
	}
	
	private void language_changed (ComboBox combobox) {
		int l = combobox.active;
		for (int c = 0; c < n_countries; c++) {
			TreeIter iter;
			country_model.iter_nth_child (out iter, null, c);
			country_model.set (iter, 3, language_countries[l * n_countries + c], -1);
		}
		
		if (country_combobox.active == -1) {
			TreeIter iter, filter_iter;
			country_model.iter_nth_child (out iter, null, language_default_country[l]);
			country_filter.convert_child_iter_to_iter (out filter_iter, iter);
			country_combobox.set_active_iter (filter_iter);
		}
		
		update_settings ();
	}
	
	private void country_changed (ComboBox combobox) {
		update_settings ();
	}
	
	private void kbd_changed (ComboBox combobox) {
		update_settings ();
	}
	
	private void tz_changed (ComboBox combobox) {
		update_settings ();
	}
	
	private void update_settings () {
		TreeIter iter;
		string language_name = _("Unknown");

		if (language_combobox.get_active_iter (out iter)) {
			string language;
			language_model.get (iter, 0, out language, 1, out language_name, -1);
			settings.language = language;
		}

		if (country_combobox.get_active_iter (out iter)) {
			string country, country_name;
			country_filter.get (iter, 0, out country, 1, out country_name, -1);
			settings.country = country;
			language_name += " (%s)".printf (country_name);
		}
		
		settings.language_name = language_name;

		if (kbd_combobox.get_active_iter (out iter)) {
			string xkblayout, xkbvariant, kbd, keyboard_name;
			kbd_model.get (iter, 0, out xkblayout, 1, out xkbvariant, 2, out kbd, 3, out keyboard_name, -1);
			settings.xkblayout = xkblayout;
			settings.xkbvariant = xkbvariant;
			settings.kbd = kbd;
			settings.keyboard_name = keyboard_name;
		}

		if (tz_combobox.get_active_iter (out iter)) {
			string timezone, timezone_name;
			tz_model.get (iter, 0, out timezone, 1, out timezone_name, -1);
			settings.timezone = timezone;
			settings.timezone_name = timezone_name;
		}
		
		settings.changed ();
	}

	private void add_language (string code, string name) {
		TreeIter iter;
		language_model.append (out iter);
		language_model.set (iter, 0, code, 1, name, -1);
		language_code_table.insert (code, n_languages);
		
		n_languages++;
	}

	private void add_country (string code, string name) {
		TreeIter iter;
		Gdk.Pixbuf pixbuf;
		country_model.append (out iter);
		pixbuf = new Gdk.Pixbuf.from_file_at_size ("/usr/share/paldo-installer/images/%s.svg".printf (code.down ()), 24, 24);
		country_model.set (iter, 0, code, 1, name, 2, pixbuf, -1);
		country_code_table.insert (code, n_countries);

		n_countries++;
	}

	private int add_keyboard_layout (string xkblayout, string xkbvariant, string kbd, string name) {
		TreeIter iter;
		kbd_model.append (out iter);
		kbd_model.set (iter, 0, xkblayout, 1, xkbvariant, 2, kbd, 3, name, -1);

		return n_keyboard_layouts++;
	}

	private int add_time_zone (string code, string name) {
		TreeIter iter;
		tz_model.append (out iter);
		tz_model.set (iter, 0, code, 1, name, -1);

		return n_timezones++;
	}

	private void add_locale (string language, string country, bool default_country = false) {
		int l = language_code_table.lookup (language);
		int c = country_code_table.lookup (country);
		language_countries[l * n_countries + c] = true;
		if (default_country) {
			language_default_country[l] = c;
		}
	}
}

