/* InstallerWindow.vala
 *
 * Copyright (C) 2007-2008  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

using GLib;
using Gtk;

public class Paldo.InstallerWindow : Assistant {
	private InstallerSettings settings;

	private WelcomePage welcome_page;
	private PartitioningPage partitioning_page;
	private SystemConfigPage system_config_page;
	private AddUserPage add_user_page;
	private ConfirmPage confirm_page;
	private InstallPage install_page;
	private SummaryPage summary_page;

	public InstallerWindow () {
		title = _("paldo Installer");
		border_width = 12;

		settings = new InstallerSettings ();

		// calculate installation size
		string installation_size;
		int exit_status = -1;
		Process.spawn_command_line_sync ("bash -c 'echo $(($(grep -h ^Installed-Size /run/squashfs/var/lib/upkg/packages/*.info | cut -d \" \" -f 2 | tr \"\\n\" \"+\" | sed -e \"s/$/0/\")))'", out installation_size, null, out exit_status);
		if (exit_status == 0) {
			// result should be between 1 GB and 10 GB
			settings.installation_size = int.parse (installation_size).clamp (1048576, 10485760);
			// add 10% to compensate for filesystem overhead
			settings.installation_size += settings.installation_size / 10;
		} else {
			// fallback to 2 GB
			settings.installation_size = 2097152;
		}

		// minimal root partition size: 0.5 - 1 GB larger than installation size (rounded to 0.5 GB)
		settings.min_root_size = (settings.installation_size / (512 * 1024) + 2) * 512 * 1024;

		set_default_size (800, 600);

		welcome_page = new WelcomePage (settings);
		append_page (welcome_page);
		set_page_title (welcome_page, _("Welcome"));
		set_page_type (welcome_page, AssistantPageType.INTRO);
		set_page_complete (welcome_page, true);

		partitioning_page = new PartitioningPage (settings);
		append_page (partitioning_page);
		set_page_title (partitioning_page, _("Partitioning"));
		set_page_type (partitioning_page, AssistantPageType.CONTENT);
		partitioning_page.complete_changed.connect ((sender, complete) => set_page_complete (sender, complete));

		system_config_page = new SystemConfigPage (settings);
		append_page (system_config_page);
		set_page_title (system_config_page, _("System Configuration"));
		set_page_type (system_config_page, AssistantPageType.CONTENT);
		system_config_page.complete_changed.connect ((sender, complete) => set_page_complete (sender, complete));

		add_user_page = new AddUserPage (settings);
		append_page (add_user_page);
		set_page_title (add_user_page, _("Add User"));
		set_page_type (add_user_page, AssistantPageType.CONTENT);
		add_user_page.complete_changed.connect ((sender, complete) => set_page_complete (sender, complete));

		confirm_page = new ConfirmPage (settings);
		append_page (confirm_page);
		set_page_title (confirm_page, _("Confirm"));
		set_page_type (confirm_page, AssistantPageType.CONFIRM);
		set_page_complete (confirm_page, true);

		install_page = new InstallPage (settings);
		append_page (install_page);
		set_page_title (install_page, _("Installation"));
		set_page_type (install_page, AssistantPageType.PROGRESS);
		install_page.complete_changed.connect ((sender, complete) => {
			set_page_complete (sender, complete);
			if (complete) {
				// installation has completed, go to summary page
				set_current_page (get_current_page () + 1);

				var restart_btn = new Button ();
				var restart_hbox = new HBox (false, 0);
				restart_btn.add (restart_hbox);
				restart_hbox.pack_start (new Image.from_stock (Gtk.Stock.REFRESH, IconSize.BUTTON), false, false, 0);
				restart_hbox.pack_start (new Label.with_mnemonic (_("_Restart")), false, false, 0);
				restart_btn.show_all ();
				add_action_widget (restart_btn);

				restart_btn.clicked.connect (on_restart_clicked);
			}
		});

		summary_page = new SummaryPage (settings);
		append_page (summary_page);
		set_page_title (summary_page, _("Summary"));
		set_page_type (summary_page, AssistantPageType.SUMMARY);
		set_page_complete (summary_page, true);

		prepare.connect (on_prepare);

		cancel.connect (on_close);
		close.connect (on_close);
	}

	private void on_restart_clicked (Button sender) {
		Process.spawn_command_line_async ("shutdown -r now");
		Gtk.main_quit ();
	}

	private void on_close (Assistant sender) {
		Gtk.main_quit ();
	}

	private void on_prepare (Assistant sender, Widget page) {
		if (page == partitioning_page) {
			partitioning_page.prepare ();
		} else if (page == install_page) {
			// apply on confirm page clicked => start installation
			install_page.run ();
		}
	}

	public static int main (string[] args) {
		Gtk.init (ref args);

		var win = new InstallerWindow ();
		win.show_all ();

		Gtk.main ();

		return 0;
	}
}

