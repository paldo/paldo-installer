/* InstallerSettings.vala
 *
 * Copyright (C) 2007  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

using GLib;

public class Paldo.InstallerSettings : GLib.Object {
	public string language { get; set; }
	public string country { get; set; }
	public string language_name { get; set; }
	public string xkblayout { get; set; }
	public string xkbvariant { get; set; }
	public string kbd { get; set; }
	public string keyboard_name { get; set; }
	public string timezone { get; set; }
	public string timezone_name { get; set; }

	// in KB
	public int installation_size { get; set; }
	public int min_root_size { get; set; }

	public string root_dev { get; set; }
	public string root_fstype { get; set; }
	public string root_label { get; set; }
	public string root_name { get; set; }
	public string boot_dev { get; set; }
	public string boot_fstype { get; set; }
	public string boot_label { get; set; }
	public string boot_name { get; set; }
	public string home_dev { get; set; }
	public string home_fstype { get; set; }
	public bool home_format { get; set; }
	public string home_label { get; set; }
	public string home_name { get; set; }
	public string swap_dev { get; set; }
	public string swap_label { get; set; }
	public string swap_name { get; set; }

	public string hostname { get; set; }
	public string domainname { get; set; }
	public string root_password { get; set; }

	public string username { get; set; }
	public int uid { get; set; }
	public string full_name { get; set; }
	public string user_password { get; set; }

	public bool frozen { get; set; }

	public InstallerSettings () {
	}

	public signal void changed ();
}

