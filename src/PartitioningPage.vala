/* PartitioningPage.vala
 *
 * Copyright (C) 2007-2008  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

using GLib;
using Gtk;

public class Paldo.PartitioningPage : VBox {
	public InstallerSettings settings { get; set; }

	private UDisks.Client udisks;

	private TreeStore storage_tree;
	private Gtk.ListStore partition_list;
	private TreeView storage_tree_view;

	private ComboBox root_combobox;
	private Image root_warning_image;
	private Label root_warning_label;
	private HBox root_warning_hbox;
	private ComboBox home_combobox;
	private Image home_warning_image;
	private Label home_warning_label;
	private HBox home_warning_hbox;
	private bool home_error;
	private ComboBox boot_combobox;
	private Image boot_warning_image;
	private Label boot_warning_label;
	private HBox boot_warning_hbox;
	private bool boot_error;
	private ComboBox swap_combobox;
	private Image swap_warning_image;
	private Label swap_warning_label;
	private HBox swap_warning_hbox;
	private bool swap_error;

	private bool complete;

	public PartitioningPage (InstallerSettings settings) {
		this.settings = settings;
		spacing = 6;
		border_width = 12;

		var section_label = new Label (_("<b>Partitions</b>"));
		section_label.use_markup = true;
		section_label.xalign = 0;
		pack_start (section_label, false, false, 0);

		var hbox = new HBox (false, 6);
		pack_start (hbox, true, true, 0);
		hbox.pack_start (new Label ("    "), false, false, 0);

		var vbox = new VBox (false, 6);
		hbox.pack_start (vbox, true, true, 0);

		var label = new Label (_("Your root partition should have a size of at least %s.").printf (get_min_root_display_size ()));
		label.xalign = 0;
		vbox.pack_start (label, false, false, 0);
		label = new Label (_("Make sure that you backup all important data before changing your partitions."));
		label.xalign = 0;
		vbox.pack_start (label, false, false, 0);

		storage_tree = new TreeStore (7, typeof (string), typeof (string), typeof (string), typeof (string), typeof (string), typeof (string), typeof (int));
		partition_list = new Gtk.ListStore (9, typeof (string), typeof (string), typeof (string), typeof (string), typeof (string), typeof (int), typeof (string), typeof (string), typeof (uint64));

		var sw = new ScrolledWindow (null, null);
		vbox.pack_start (sw, true, true, 0);
		sw.shadow_type = ShadowType.ETCHED_IN;
		sw.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);

		storage_tree_view = new TreeView.with_model (storage_tree);
		sw.add (storage_tree_view);
		storage_tree_view.headers_visible = false;
		section_label.mnemonic_widget = storage_tree_view;

		var col = new TreeViewColumn ();
		var renderer_pixbuf = new CellRendererPixbuf ();
		col.pack_start (renderer_pixbuf, false);
		col.add_attribute (renderer_pixbuf, "icon-name", 5);
		var renderer = new CellRendererText ();
		col.pack_start (renderer, true);
		col.add_attribute (renderer, "text", 2);
		storage_tree_view.append_column (col);

		col = new TreeViewColumn ();
		renderer = new CellRendererText ();
		col.pack_start (renderer, true);
		col.add_attribute (renderer, "text", 1);
		storage_tree_view.append_column (col);

		col = new TreeViewColumn ();
		renderer = new CellRendererText ();
		col.pack_start (renderer, true);
		renderer.xalign = 1;
		col.add_attribute (renderer, "text", 3);
		storage_tree_view.append_column (col);

		col = new TreeViewColumn ();
		renderer = new CellRendererText ();
		col.pack_start (renderer, true);
		col.add_attribute (renderer, "text", 4);
		storage_tree_view.append_column (col);

		var hbuttonbox = new HButtonBox ();
		vbox.pack_start (hbuttonbox, false, false, 0);
		hbuttonbox.layout_style = ButtonBoxStyle.END;
		var button = new Button.from_stock (Gtk.Stock.EDIT);
		hbuttonbox.pack_start (button, false, false, 0);
		button.clicked.connect (edit_clicked);

		var table = new Table (8, 3, false);
		pack_start (table, false, false, 0);
		table.set_col_spacings (6);
		table.set_row_spacings (6);

		table.attach (new Label ("    "), 0, 1, 0, 4, (AttachOptions) 0, (AttachOptions) 0, 0, 0);

		label = new Label (_("Boot partition:"));
		label.use_markup = true;
		label.xalign = 0;
		table.attach (label, 1, 2, 0, 1, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		boot_combobox = new ComboBox.with_model (partition_list);
		table.attach (boot_combobox, 2, 3, 0, 1, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		renderer_pixbuf = new CellRendererPixbuf ();
		boot_combobox.pack_start (renderer_pixbuf, false);
		boot_combobox.add_attribute (renderer_pixbuf, "icon-name", 4);
		renderer = new CellRendererText ();
		boot_combobox.pack_start (renderer, true);
		boot_combobox.add_attribute (renderer, "text", 2);
		label.mnemonic_widget = boot_combobox;
		boot_warning_hbox = new HBox (false, 6);
		table.attach (boot_warning_hbox, 2, 3, 1, 2, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		boot_warning_hbox.pack_start (new Label (""), false, false, 0);
		boot_warning_image = new Image ();
		boot_warning_hbox.pack_start (boot_warning_image, false, false, 0);
		boot_warning_label = new Label ("");
		boot_warning_label.xalign = 0;
		boot_warning_hbox.pack_start (boot_warning_label, false, false, 0);

		label = new Label (_("Root partition:"));
		label.xalign = 0;
		table.attach (label, 1, 2, 2, 3, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		root_combobox = new ComboBox.with_model (partition_list);
		table.attach (root_combobox, 2, 3, 2, 3, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		renderer_pixbuf = new CellRendererPixbuf ();
		root_combobox.pack_start (renderer_pixbuf, false);
		root_combobox.add_attribute (renderer_pixbuf, "icon-name", 4);
		renderer = new CellRendererText ();
		root_combobox.pack_start (renderer, true);
		root_combobox.add_attribute (renderer, "text", 2);
		label.mnemonic_widget = root_combobox;
		root_warning_hbox = new HBox (false, 6);
		table.attach (root_warning_hbox, 2, 3, 3, 4, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		root_warning_hbox.pack_start (new Label (""), false, false, 0);
		root_warning_image = new Image ();
		root_warning_hbox.pack_start (root_warning_image, false, false, 0);
		root_warning_label = new Label ("");
		root_warning_label.xalign = 0;
		root_warning_hbox.pack_start (root_warning_label, false, false, 0);

		label = new Label (_("Home partition:\n<small>(optional)</small>"));
		label.use_markup = true;
		label.xalign = 0;
		table.attach (label, 1, 2, 4, 5, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		home_combobox = new ComboBox.with_model (partition_list);
		table.attach (home_combobox, 2, 3, 4, 5, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		renderer_pixbuf = new CellRendererPixbuf ();
		home_combobox.pack_start (renderer_pixbuf, false);
		home_combobox.add_attribute (renderer_pixbuf, "icon-name", 4);
		renderer = new CellRendererText ();
		home_combobox.pack_start (renderer, true);
		home_combobox.add_attribute (renderer, "text", 2);
		label.mnemonic_widget = home_combobox;
		home_warning_hbox = new HBox (false, 6);
		table.attach (home_warning_hbox, 2, 3, 5, 6, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		home_warning_hbox.pack_start (new Label (""), false, false, 0);
		home_warning_image = new Image ();
		home_warning_hbox.pack_start (home_warning_image, false, false, 0);
		home_warning_label = new Label ("");
		home_warning_label.xalign = 0;
		home_warning_hbox.pack_start (home_warning_label, false, false, 0);

		label = new Label (_("Swap partition:\n<small>(optional)</small>"));
		label.use_markup = true;
		label.xalign = 0;
		table.attach (label, 1, 2, 6, 7, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		swap_combobox = new ComboBox.with_model (partition_list);
		table.attach (swap_combobox, 2, 3, 6, 7, AttachOptions.EXPAND | AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		renderer_pixbuf = new CellRendererPixbuf ();
		swap_combobox.pack_start (renderer_pixbuf, false);
		swap_combobox.add_attribute (renderer_pixbuf, "icon-name", 4);
		renderer = new CellRendererText ();
		swap_combobox.pack_start (renderer, true);
		swap_combobox.add_attribute (renderer, "text", 2);
		label.mnemonic_widget = swap_combobox;
		swap_warning_hbox = new HBox (false, 6);
		table.attach (swap_warning_hbox, 2, 3, 7, 8, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		swap_warning_hbox.pack_start (new Label (""), false, false, 0);
		swap_warning_image = new Image ();
		swap_warning_hbox.pack_start (swap_warning_image, false, false, 0);
		swap_warning_label = new Label ("");
		swap_warning_label.xalign = 0;
		swap_warning_hbox.pack_start (swap_warning_label, false, false, 0);

		root_combobox.changed.connect (partition_changed);
		home_combobox.changed.connect (partition_changed);
		boot_combobox.changed.connect (partition_changed);
		swap_combobox.changed.connect (partition_changed);

		udisks = new UDisks.Client.sync ();
		udisks.changed.connect (() => {
			on_udisks_changed ();
		});
		on_udisks_changed ();
	}

	private void edit_clicked (Button widget) {
		Process.spawn_command_line_async ("gnome-disks");
	}

	void on_udisks_changed () {
		storage_tree.clear ();
		partition_list.clear ();

		TreeIter iter;
		partition_list.insert_with_values (out iter, 0, 2, _("(None)"), -1);

		root_combobox.active = 0;
		home_combobox.active = 0;
		boot_combobox.active = 0;
		swap_combobox.active = 0;

		var object_manager = udisks.object_manager;
		foreach (var object in object_manager.get_objects ()) {
			var udisks_object = (UDisks.Object) object;
			string path = object.get_object_path ();

			var drive = udisks_object.get_drive ();
			if (drive != null && !drive.optical && drive.size > 0) {
				var block = udisks.get_block_for_drive (drive, false);
				var block_object = (UDisks.Object) ((DBusInterface) block).get_object ();

				string dev = block.preferred_device;
				string name;
				udisks.get_drive_info (drive, out name, null, null, null, null);
				string size_string = udisks.get_size_for_display (drive.size, false, false);

				bool has_child = storage_tree.iter_children (out iter, null);
				int index = 0;
				while (has_child) {
					string it_path;
					storage_tree.get (iter, 0, out it_path, -1);
					if (it_path == path) {
						// device already in list
						return;
					}
					if (path < it_path) {
						// insert before iterator pos
						break;
					}
					has_child = storage_tree.iter_next (ref iter);
					index++;
				}

				storage_tree.insert_with_values (out iter, null, index, 0, path, 1, dev, 2, name, 3, size_string, 5, "drive-harddisk", -1);

				var partition_table = block_object.get_partition_table ();
				if (partition_table != null) {
					foreach (var partition in udisks.get_partitions (partition_table)) {
						var partition_object = (UDisks.Object) ((DBusInterface) partition).get_object ();
						string partition_path = partition_object.get_object_path ();
						var partition_block = partition_object.get_block ();

						string devp = partition_block.preferred_device;
						string partition_name = udisks.get_partition_info (partition);
						string partition_type = udisks.get_id_for_display (partition_block.id_usage, partition_block.id_type, partition_block.id_version, false);
						string partition_size_string = udisks.get_size_for_display (partition.size, false, false);
						uint part_number = partition.number;

						string details;
						if (partition_type != "") {
							details = "%s, %s, %s".printf (devp, partition_size_string, partition_type);
						} else {
							details = "%s, %s".printf (devp, partition_size_string);
						}
						string list_name = "%s on %s (%s)".printf (partition_name, name, details);


						if (partition.size < 1024 * 1024) {
							// very small partition, possibly just an extended partition, ignore
							continue;
						}

						TreeIter part_iter;
						has_child = storage_tree.iter_children (out part_iter, iter);
						index = 0;
						while (has_child) {
							int it_part_number;
							storage_tree.get (part_iter, 6, out it_part_number, -1);
							if (it_part_number == part_number) {
								// device already in list
								return;
							}
							if (part_number < it_part_number) {
								// insert before iterator pos
								break;
							}
							has_child = storage_tree.iter_next (ref part_iter);
							index++;
						}

						storage_tree.insert_with_values (out part_iter, iter, index, 0, partition_path, 1, devp, 2, partition_name, 3, partition_size_string, 4, partition_type, 5, "drive-harddisk", 6, part_number, -1);


						has_child = partition_list.iter_nth_child (out part_iter, null, 1);
						index = 1;
						while (has_child) {
							string it_path;
							int it_part_number;
							partition_list.get (part_iter, 6, out it_path, 5, out it_part_number, -1);
							if (path == it_path) {
								if (it_part_number == part_number) {
									// device already in list
									return;
								}
								if (part_number < it_part_number) {
									// insert before iterator pos
									break;
								}
							}
							if (path < it_path) {
								// insert before iterator pos
								break;
							}
							has_child = partition_list.iter_next (ref part_iter);
							index++;
						}

						partition_list.insert_with_values (out part_iter, index, 0, partition_path, 1, devp, 2, list_name, 3, partition_type, 4, "drive-harddisk", 5, part_number, 6, path, 8, partition.size, -1);

						storage_tree_view.expand_all ();
					}
				}
			}
		}
	}

	private void partition_changed (ComboBox widget) {
		setting_changed ();
	}

	private string get_min_root_display_size () {
		int half_gb_count = settings.min_root_size / (512 * 1024);
		return "%d%s GB".printf (half_gb_count / 2, half_gb_count % 2 == 0 ? "" : ".5");
	}

	private void setting_changed () {
		if (settings.frozen) {
			return;
		}

		string disk_path = null;
		if (boot_combobox.active > 0) {
			TreeIter iter;
			boot_combobox.get_active_iter (out iter);
			partition_list.get (iter, 6, out disk_path, -1);
		} else if (root_combobox.active > 0) {
			TreeIter iter;
			root_combobox.get_active_iter (out iter);
			partition_list.get (iter, 6, out disk_path, -1);
		}

		update_partition_settings ();

		// settings are ok if root and boot partitions have been chosen and no volume has been chosen in multiple partition comboboxes
		bool new_complete = settings.root_dev != null && settings.boot_dev != null && !home_error && !boot_error && !swap_error;
		if (complete != new_complete) {
			complete = new_complete;
			complete_changed (complete);
		}
	}

	private void update_partition_settings () {
		TreeIter iter;
		string dev, fstype, label, name;
		uint64 size;

		dev = null;
		fstype = null;
		label = null;
		name = null;
		size = 0;
		if (root_combobox.get_active_iter (out iter)) {
			partition_list.get (iter, 1, out dev, 3, out fstype, 2, out name, 7, out label, 8, out size, -1);
			if (fstype == null || (fstype != "ext2" && fstype != "ext3" && fstype != "ext4" && fstype != "reiserfs" && fstype != "xfs")) {
				fstype = "ext4";
			}
		}
		settings.root_dev = dev;
		settings.root_fstype = fstype;
		settings.root_label = label;
		settings.root_name = name;
		if (settings.root_dev != null) {
			if (size < (uint64) settings.min_root_size * 1024) {
				settings.root_dev = null;
				root_warning_image.set_from_stock (Gtk.Stock.DIALOG_ERROR, IconSize.MENU);
				root_warning_label.label = _("The root partition should have a size of at least %s.").printf (get_min_root_display_size ());
			} else {
				root_warning_image.set_from_stock (Gtk.Stock.DIALOG_WARNING, IconSize.MENU);
				root_warning_label.label = _("All data on the root partition will be destroyed.");
			}
			root_warning_hbox.visible = true;
		} else {
			root_warning_hbox.visible = false;
		}

		dev = null;
		fstype = null;
		label = null;
		name = null;
		settings.home_format = false;
		if (home_combobox.get_active_iter (out iter)) {
			partition_list.get (iter, 1, out dev, 3, out fstype, 2, out name, 7, out label, -1);
			if (fstype == null || (fstype != "ext2" && fstype != "ext3" && fstype != "ext4" && fstype != "reiserfs" && fstype != "xfs")) {
				fstype = "ext4";
				settings.home_format = true;
			}
		}
		settings.home_dev = dev;
		settings.home_fstype = fstype;
		settings.home_label = label;
		settings.home_name = name;
		home_error = false;
		if (settings.home_dev != null && settings.root_dev != null && settings.home_dev == settings.root_dev) {
			home_warning_image.set_from_stock (Gtk.Stock.DIALOG_ERROR, IconSize.MENU);
			home_warning_label.label = _("You may not choose the same partition for root and home.");
			home_warning_hbox.visible = true;
			home_error = true;
		} else if (settings.home_dev != null && settings.home_format) {
			home_warning_image.set_from_stock (Gtk.Stock.DIALOG_WARNING, IconSize.MENU);
			home_warning_label.label = _("All data on the home partition will be destroyed.");
			home_warning_hbox.visible = true;
		} else {
			home_warning_hbox.visible = false;
		}

		dev = null;
		fstype = null;
		label = null;
		name = null;
		if (boot_combobox.get_active_iter (out iter)) {
			partition_list.get (iter, 1, out dev, 3, out fstype, 2, out name, 7, out label, -1);
			fstype = "vfat";
		}
		settings.boot_dev = dev;
		settings.boot_fstype = fstype;
		settings.boot_label = label;
		settings.boot_name = name;
		boot_error = false;
		if (settings.boot_dev != null && settings.root_dev != null && settings.boot_dev == settings.root_dev) {
			boot_warning_image.set_from_stock (Gtk.Stock.DIALOG_ERROR, IconSize.MENU);
			boot_warning_label.label = _("You may not choose the same partition for root and boot.");
			boot_warning_hbox.visible = true;
			boot_error = true;
		} else if (settings.boot_dev != null && settings.home_dev != null && settings.boot_dev == settings.home_dev) {
			boot_warning_image.set_from_stock (Gtk.Stock.DIALOG_ERROR, IconSize.MENU);
			boot_warning_label.label = _("You may not choose the same partition for home and boot.");
			boot_warning_hbox.visible = true;
			boot_error = true;
		} else if (settings.boot_dev != null) {
			boot_warning_image.set_from_stock (Gtk.Stock.DIALOG_WARNING, IconSize.MENU);
			boot_warning_label.label = _("All data on the boot partition will be destroyed.");
			boot_warning_hbox.visible = true;
		} else {
			boot_warning_hbox.visible = false;
		}

		dev = null;
		fstype = null;
		label = null;
		name = null;
		if (swap_combobox.get_active_iter (out iter)) {
			partition_list.get (iter, 1, out dev, 3, out fstype, 2, out name, 7, out label, -1);
		}
		settings.swap_dev = dev;
		settings.swap_label = label;
		settings.swap_name = name;
		swap_error = false;
		if (settings.swap_dev != null && settings.root_dev != null && settings.swap_dev == settings.root_dev) {
			swap_warning_image.set_from_stock (Gtk.Stock.DIALOG_ERROR, IconSize.MENU);
			swap_warning_label.label = _("You may not choose the same partition for root and swap.");
			swap_warning_hbox.visible = true;
			swap_error = true;
		} else if (settings.swap_dev != null && settings.home_dev != null && settings.swap_dev == settings.home_dev) {
			swap_warning_image.set_from_stock (Gtk.Stock.DIALOG_ERROR, IconSize.MENU);
			swap_warning_label.label = _("You may not choose the same partition for home and swap.");
			swap_warning_hbox.visible = true;
			swap_error = true;
		} else if (settings.swap_dev != null && settings.boot_dev != null && settings.swap_dev == settings.boot_dev) {
			swap_warning_image.set_from_stock (Gtk.Stock.DIALOG_ERROR, IconSize.MENU);
			swap_warning_label.label = _("You may not choose the same partition for boot and swap.");
			swap_warning_hbox.visible = true;
			swap_error = true;
		} else if (settings.swap_dev != null) {
			swap_warning_image.set_from_stock (Gtk.Stock.DIALOG_WARNING, IconSize.MENU);
			swap_warning_label.label = _("All data on the swap partition will be destroyed.");
			swap_warning_hbox.visible = true;
		} else {
			swap_warning_hbox.visible = false;
		}

		settings.changed ();
	}

	public signal void complete_changed (bool complete);

	public void prepare () {
		setting_changed ();
	}
}

