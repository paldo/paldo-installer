/* ConfirmPage.vala
 *
 * Copyright (C) 2007  Jürg Billeter
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 * Author:
 * 	Jürg Billeter <j@bitron.ch>
 */

using GLib;
using Gtk;

public class Paldo.ConfirmPage : VBox {
	public InstallerSettings settings { get; set; }

	private Label language_label;
	private Label keyboard_label;
	private Label timezone_label;

	private Label boot_label;
	private Label root_label;
	private Label home_label;
	private Label swap_label;

	private HBox boot_warning;
	private HBox home_warning;
	private HBox swap_warning;

	private Label hostname_label;

	private Label username_label;

	public ConfirmPage (InstallerSettings settings) {
		this.settings = settings;
		spacing = 6;
		border_width = 12;

		Label label;

		var table = new Table (14, 3, false);
		pack_start (table, false, false, 0);
		table.set_col_spacings (6);
		table.set_row_spacings (6);

		label = new Label (_("<b>Locale Settings</b>"));
		label.use_markup = true;
		label.xalign = 0;
		table.attach (label, 0, 3, 0, 1, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		table.attach (new Label ("    "), 0, 1, 1, 4, (AttachOptions) 0, (AttachOptions) 0, 0, 0);

		label = new Label (_("Language:"));
		label.xalign = 0;
		table.attach (label, 1, 2, 1, 2, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		language_label = new Label ("");
		language_label.xalign = 0;
		table.attach (language_label, 2, 3, 1, 2, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		label = new Label (_("Keyboard layout:"));
		label.xalign = 0;
		table.attach (label, 1, 2, 2, 3, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		keyboard_label = new Label ("");
		keyboard_label.xalign = 0;
		table.attach (keyboard_label, 2, 3, 2, 3, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		label = new Label (_("Time zone:"));
		label.xalign = 0;
		table.attach (label, 1, 2, 3, 4, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		timezone_label = new Label ("");
		timezone_label.xalign = 0;
		table.attach (timezone_label, 2, 3, 3, 4, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		label = new Label (_("<b>Partitions</b>"));
		label.use_markup = true;
		label.xalign = 0;
		table.attach (label, 0, 3, 4, 5, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		table.attach (new Label ("    "), 0, 1, 1, 4, (AttachOptions) 0, (AttachOptions) 0, 0, 0);

		label = new Label (_("Boot partition:"));
		label.xalign = 0;
		label.yalign = 0;
		table.attach (label, 1, 2, 5, 6, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		var vbox = new VBox (false, 6);
		table.attach (vbox, 2, 3, 5, 6, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		boot_label = new Label ("");
		boot_label.xalign = 0;
		vbox.pack_start (boot_label, false, false, 0);
		boot_warning = new HBox (false, 6);
		vbox.pack_start (boot_warning, false, false, 0);
		var img = new Image.from_stock (Gtk.Stock.DIALOG_WARNING, IconSize.MENU);
		boot_warning.pack_start (img, false, false, 0);
		label = new Label (_("All data on the boot partition will be destroyed."));
		label.xalign = 0;
		boot_warning.pack_start (label, false, false, 0);

		label = new Label (_("Root partition:"));
		label.xalign = 0;
		label.yalign = 0;
		table.attach (label, 1, 2, 6, 7, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		vbox = new VBox (false, 6);
		table.attach (vbox, 2, 3, 6, 7, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		root_label = new Label ("");
		root_label.xalign = 0;
		vbox.pack_start (root_label, false, false, 0);
		var hbox = new HBox (false, 6);
		vbox.pack_start (hbox, false, false, 0);
		img = new Image.from_stock (Gtk.Stock.DIALOG_WARNING, IconSize.MENU);
		hbox.pack_start (img, false, false, 0);
		label = new Label (_("All data on the root partition will be destroyed."));
		label.xalign = 0;
		hbox.pack_start (label, false, false, 0);

		label = new Label (_("Home partition:"));
		label.xalign = 0;
		label.yalign = 0;
		table.attach (label, 1, 2, 7, 8, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		vbox = new VBox (false, 6);
		table.attach (vbox, 2, 3, 7, 8, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		home_label = new Label ("");
		home_label.xalign = 0;
		vbox.pack_start (home_label, false, false, 0);
		home_warning = new HBox (false, 6);
		vbox.pack_start (home_warning, false, false, 0);
		img = new Image.from_stock (Gtk.Stock.DIALOG_WARNING, IconSize.MENU);
		home_warning.pack_start (img, false, false, 0);
		label = new Label (_("All data on the home partition will be destroyed."));
		label.xalign = 0;
		home_warning.pack_start (label, false, false, 0);

		label = new Label (_("Swap partition:"));
		label.xalign = 0;
		label.yalign = 0;
		table.attach (label, 1, 2, 8, 9, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		vbox = new VBox (false, 6);
		table.attach (vbox, 2, 3, 8, 9, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		swap_label = new Label ("");
		swap_label.xalign = 0;
		vbox.pack_start (swap_label, false, false, 0);
		swap_warning = new HBox (false, 6);
		vbox.pack_start (swap_warning, false, false, 0);
		img = new Image.from_stock (Gtk.Stock.DIALOG_WARNING, IconSize.MENU);
		swap_warning.pack_start (img, false, false, 0);
		label = new Label (_("All data on the swap partition will be destroyed."));
		label.xalign = 0;
		swap_warning.pack_start (label, false, false, 0);

		label = new Label (_("<b>System Configuration</b>"));
		label.use_markup = true;
		label.xalign = 0;
		table.attach (label, 0, 3, 10, 11, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		table.attach (new Label ("    "), 0, 1, 10, 11, (AttachOptions) 0, (AttachOptions) 0, 0, 0);

		label = new Label (_("Host name:"));
		label.xalign = 0;
		table.attach (label, 1, 2, 11, 12, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		hostname_label = new Label ("");
		hostname_label.xalign = 0;
		table.attach (hostname_label, 2, 3, 11, 12, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		label = new Label (_("<b>User</b>"));
		label.use_markup = true;
		label.xalign = 0;
		table.attach (label, 0, 3, 12, 13, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		table.attach (new Label ("    "), 0, 1, 12, 13, (AttachOptions) 0, (AttachOptions) 0, 0, 0);

		label = new Label (_("User name:"));
		label.xalign = 0;
		table.attach (label, 1, 2, 13, 14, AttachOptions.FILL, AttachOptions.FILL, 0, 0);
		username_label = new Label ("");
		username_label.xalign = 0;
		table.attach (username_label, 2, 3, 13, 14, AttachOptions.FILL, AttachOptions.FILL, 0, 0);

		settings.changed.connect (settings_changed);
	}

	private void settings_changed (InstallerSettings settings) {
		if (settings.language_name != null) {
			language_label.label = settings.language_name;
		}
		if (settings.keyboard_name != null) {
			keyboard_label.label = settings.keyboard_name;
		}
		if (settings.timezone_name != null) {
			timezone_label.label = settings.timezone_name;
		}

		if (settings.root_name != null) {
			root_label.label = settings.root_name;
		}
		if (settings.home_dev != null) {
			home_label.label = settings.home_name;
			home_warning.visible = settings.home_format;
		} else {
			home_label.label = "(None)";
			home_warning.hide ();
		}
		if (settings.boot_name != null) {
			boot_label.label = settings.boot_name;
		}
		if (settings.swap_dev != null) {
			swap_label.label = settings.swap_name;
			swap_warning.show ();
		} else {
			swap_label.label = "(None)";
			swap_warning.hide ();
		}

		if (settings.hostname != null && settings.domainname != null) {
			hostname_label.label = "%s.%s".printf (settings.hostname, settings.domainname);
		}

		if (settings.username != null) {
			username_label.label = settings.username;
		}
	}
}

